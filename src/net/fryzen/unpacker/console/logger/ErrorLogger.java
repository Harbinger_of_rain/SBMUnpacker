/*
    author: Harbinger_of_rain
 */

package net.fryzen.unpacker.console.logger;

import java.util.ArrayList;
import java.util.List;

public class ErrorLogger implements Logger {
    List<String> errorList = new ArrayList<>();

    public void add(String error) {
        this.errorList.add(error);
    }

    public void addMultiple(List<String> errors) {
        this.errorList.addAll(errors);
    }

    public List<String> getAll() {
        return errorList;
    }
}
