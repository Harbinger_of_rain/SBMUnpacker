/*
    author: Harbinger_of_rain
 */

package net.fryzen.unpacker.console.logger;

import java.util.List;

public interface Logger {
    void add(String log);
    void addMultiple(List<String> errors);
    List<String> getAll();
}
