/*
    author: Harbinger_of_rain
 */

package net.fryzen.unpacker.console;

import net.fryzen.unpacker.console.logger.Logger;
import org.apache.commons.io.filefilter.RegexFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Unpacker extends Observable {
    private Executor executor = new Executor();
    private FileOperator fileOperator;
    private Logger errLogger;
    private FileFilter fileFilterPak = new RegexFileFilter("[0-9A-Za-z_.-]*\\.pak");

    private int modAmount;
    private int modCounter;
    
    private String winUnpak = "win32/asset_unpacker.exe";
    private String linUnpak = "linux/asset_unpacker";
    private String unpacker = "";
    private Path modDir = Paths.get("mods");
    private Path outputPath = Paths.get("modsUnpacked");
    private Path errorPath = Paths.get("modsError");

    private File outputFile = new File(outputPath.toString());
    private File errorFile = new File(errorPath.toString());
    private File[] modFiles = fileOperator.listSubDirs(modDir);

    public Unpacker(FileOperator fileOperator, Logger errLogger) {
        this.fileOperator = fileOperator;
        this.errLogger = errLogger;
        modAmount = 0;
        modCounter = 0;
    }

    public Logger getErrLogger() {
        return errLogger;
    }

    public void run() {
        if (!readyToRun())
            return;

        modAmount = modDir.toFile().list().length;

        fileOperator.deleteDir(outputFile);
        fileOperator.createDir(outputPath);
        fileOperator.deleteDir(errorFile);
        fileOperator.createDir(errorPath);

        for (File currentMod : modFiles) {
            File[] modContents = currentMod.listFiles();
            File[] modPaks = currentMod.listFiles(fileFilterPak);
            int allowedFiles = 1;
            File pakFile;

            modCounter++;
            setChanged();
            notifyObservers(this);

            //skip empty directories
            if (currentMod.listFiles().length == 0)
                continue;


            //TODO: copy paks to unpacked folder, after executing delete it
            //instead of executing in mods dir (no residue after error or cancellation)
//            if (modPaks.length == 1)
//                fileOperator.copyFile(currentMod);

            for (int i = 0; i > modContents.length+10; i++) {
                if (modContents[i].getName().equals(".DS_Store")) {
                    allowedFiles++;
                }
            }

            if (modContents.length != allowedFiles || modPaks.length != 1) {
                fileOperator.copyDir(currentMod, new File(errorPath + "/" + fileOperator.getName(currentMod)));
                errLogger.add(currentMod + " moved to " + errorPath + "/" + fileOperator.getName(currentMod) + " due to Error");
                continue;
            }
            pakFile = modPaks[0];

            unpack(pakFile);

            if (!Files.exists(Paths.get(modDir + "/" + fileOperator.getName(currentMod) + "/" + fileOperator.getName(currentMod)))) {
                errLogger.add("unpacking of " + modDir + "/" + fileOperator.getName(currentMod) + "/" + fileOperator.getName(currentMod) + " failed!");
            } else
            fileOperator.moveDir(
                    Paths.get(modDir + "/" + fileOperator.getName(currentMod) + "/" + fileOperator.getName(currentMod)),
                    Paths.get(outputPath + "/" + fileOperator.getName(currentMod))
            );
        }
    }

    public boolean readyToRun() {
        boolean ready = true;
        String os = System.getProperty("os.name");

        if (os.contains("Windows") || os.contains("windows"))
            unpacker = winUnpak;
        else if (os.contains("nix") || os.contains("nux"))
            unpacker = linUnpak;
        else {
            errLogger.add("Your OS wasn't recognised... exiting...");
            ready = false;
        }
        if (!Files.exists(modDir)) {
            errLogger.add("Directory \"" + modDir + "\" needs to exist! exiting...");
            ready = false;
        }
        if (!Files.exists(Paths.get(winUnpak))) {
            errLogger.add("Directory \"" + winUnpak + "\" needs to exist! exiting...");
            ready = false;
        }

        return ready;
    }

    private void unpack(File pakFile){
        List<String> errors = new ArrayList<>();
        String pakPath = pakFile.toString();
        String outputDir = pakFile.getParent() + "/" + pakFile.getParentFile().getName();


        errors = executor.execute(unpacker ,pakPath ,outputDir);
        errLogger.addMultiple(errors);
    }

    public int getAmount() {
        return modAmount;
    }
    public int getCounter() {
        return modCounter;
    }
}
