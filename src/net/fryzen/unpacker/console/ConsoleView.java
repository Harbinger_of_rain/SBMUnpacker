/*
    author: Harbinger_of_rain
 */

package net.fryzen.unpacker.console;

import net.fryzen.unpacker.console.logger.Logger;

import java.util.Observable;
import java.util.Observer;

public class ConsoleView implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        int amount = ((Unpacker)o).getAmount();
        int counter = ((Unpacker)o).getCounter();
        System.out.print("("+ counter + "/" + amount + ") ");
        System.out.println(makeBar(amount, counter));
//        clear();
    }

    public void printErrors(String source, Logger errors) {
        System.out.println("Errors from " + source + ":");
        for (String error : errors.getAll()) {
            System.out.println("     " + source + ": " + error);
        }
        System.out.println();
    }

    public void finish() {
        clear();
        System.out.println("Done! Please check the error Folder for mods that have to be dealt with manually.");
        System.out.println();
    }

    public String makeBar(int modAmount, int modCount) {
        String barStr = "";
        int barMax = 50;
        float modPerc = (float)modCount / modAmount;
        int barLen = Math.round(modPerc * barMax); //TODO: move this logic to model
        barStr += "|";
        for (int i = 0; i < barLen; i++){
            barStr += "=";}
        for (int i = 0; i < barMax-barLen; i++){
            barStr += " ";}
        barStr += "|";
        return barStr;
    }

    private void clear() {
        for (int i = 0; i < 20; i++)
            System.out.println();
    }
}
